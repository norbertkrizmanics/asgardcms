<?php

namespace Modules\Testimonials\Entities;

use Illuminate\Database\Eloquent\Model;

class Testimonial extends Model
{

    protected $table = 'testimonials__testimonials';
    protected $fillable = [
        'name',
        'url',
        'content',
    ];
}
