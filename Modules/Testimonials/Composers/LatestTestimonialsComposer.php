<?php
/**
 * Created by PhpStorm.
 * User: krizmanicsnorbert
 * Date: 2018. 09. 25.
 * Time: 11:28
 */

namespace Modules\Testimonials\Composers;


use Illuminate\Contracts\View\View;
use Modules\Testimonials\Repositories\TestimonialRepository;

class LatestTestimonialsComposer
{
    private $testimonials;

    public function __construct(TestimonialRepository $testimonials)
    {
        $this->testimonials=$testimonials;
    }

    public function compose(View $view){
        $view->with('randomTestimonials',$this->testimonials->randomTestimonials());
    }
}