<?php

namespace Modules\Testimonials\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateTestimonialRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'name'=>'required',
            'url'=>'required',
            'content'=>'required',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
