@extends('layouts.master')

@section('title')
    {{ $page->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')
    <div class="row">
        <h1>{{ $page->title }}</h1>
        {!! $page->body !!}
    </div>
    <div class="container text-center">
            <div class="row">
                @foreach ($randomTestimonials as $testimonial)

                <div class="col-md-4">
                    <h1>{{$testimonial->name}}</h1>
                    <h3><a href="{{$testimonial->url}}">URL</a></h3>
                    <p>{{$testimonial->content}}</p>
                </div>
                @endforeach

            </div>

    </div>
@stop
