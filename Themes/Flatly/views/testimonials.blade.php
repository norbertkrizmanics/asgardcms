@extends('layouts.master')

@section('title')
    {{ $page->title }} | @parent
@stop
@section('meta')
    <meta name="title" content="{{ $page->meta_title}}" />
    <meta name="description" content="{{ $page->meta_description }}" />
@stop

@section('content')
    <div class="row">
        <h1>{{ $page->title }}</h1>
        {!! $page->body !!}
    </div>
<div class="container">
    <div class="row">
        <div class="col-md-4">TEST</div>
        <div class="col-md-4">TEST</div>
        <div class="col-md-4">TEST</div>
    </div>
</div>
@stop
